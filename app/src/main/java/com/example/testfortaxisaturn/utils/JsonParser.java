package com.example.testfortaxisaturn.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

    private static final String ATTR_API_TOKEN = "api_token";
    private static final String ATTR_API_TOKEN_EXP_DATE = "api_token_expiration_date";
    private static final String ATTR_DATA = "data";

    private String apiToken;
    private String apiTokenExpDate;

    private String jsonStr;

    public JsonParser(String jsonStr){
        this.jsonStr = jsonStr;
        parse();
    }

    private void parse(){
        try {
            JSONObject json = new JSONObject(jsonStr);
            if( json.has(ATTR_DATA) ){
                JSONObject dataObj = json.getJSONObject(ATTR_DATA);
                if( dataObj.has(ATTR_API_TOKEN) ){
                    apiToken = dataObj.getString(ATTR_API_TOKEN);
                }

                if( dataObj.has(ATTR_API_TOKEN_EXP_DATE) ){
                    apiTokenExpDate = dataObj.getString(ATTR_API_TOKEN_EXP_DATE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getApiToken(){
        return apiToken;
    }

    public String getApiTokenExpDate(){
        return apiTokenExpDate;
    }
}