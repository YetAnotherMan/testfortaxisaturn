package com.example.testfortaxisaturn.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pohilko on 20.08.2015.
 */
public class Preferences {

    private static final String PREF_FILENAME = "prefs";

    private static final String ITEM_API_TOKEN = "api_token";
    private static final String ITEM_API_TOKEN_EXP_DATE = "api_token_exp_date";

    private static SharedPreferences prefs;
    private static SharedPreferences.Editor editor;

    private static Preferences preferences;

    public static Preferences getInstance(Context context){
        if( preferences == null )
            preferences = new Preferences(context);

        return preferences;
    }

    private Preferences(Context context){
        prefs = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setApiToken(String apiToken){
        editor.putString(ITEM_API_TOKEN, apiToken);
        editor.commit();
    }

    public String getApiToken(){
        return prefs.getString(ITEM_API_TOKEN, "");
    }

    public void setApiTokenExpirationDate(String apiTokenExpirationDate){
        editor.putString(ITEM_API_TOKEN_EXP_DATE, apiTokenExpirationDate);
        editor.commit();
    }

    public String getApiTokenExpirationDate(){
        return prefs.getString(ITEM_API_TOKEN_EXP_DATE, "");
    }
}