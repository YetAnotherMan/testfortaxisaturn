package com.example.testfortaxisaturn.ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testfortaxisaturn.R;
import com.example.testfortaxisaturn.api.ApiClient;
import com.example.testfortaxisaturn.utils.JsonParser;
import com.example.testfortaxisaturn.utils.Preferences;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    @Email(messageResId = R.string.err_validation_email)
    @InjectView(R.id.etEmail)
    EditText etEmail;

    @NotEmpty
    @Password
    @InjectView(R.id.etPassword)
    EditText etPassword;

    private Validator validator;
    private Preferences preferences;

    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        handler = new Handler();
        preferences = Preferences.getInstance(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }


    @OnClick(R.id.btnLogin)
    public void loginClick(){
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String email = etEmail.getText().toString();
        String pass = etPassword.getText().toString();
        ApiClient.login(email, pass, new ApiClient.ResponseListener() {
            @Override
            public void onSuccess(String json) {
                JsonParser parser = new JsonParser(json);
                String apiToken = parser.getApiToken();
                String apiTokenExpDate = parser.getApiTokenExpDate();

                preferences.setApiToken(apiToken);
                preferences.setApiTokenExpirationDate(apiTokenExpDate);

                showSuccessToast();

                finish();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

            @Override
            public void onFailure() { //stuff
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showSuccessToast(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, R.string.successful_login, Toast.LENGTH_SHORT).show();
            }
        });
    }
}