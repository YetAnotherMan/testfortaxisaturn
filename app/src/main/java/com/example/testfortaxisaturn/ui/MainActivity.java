package com.example.testfortaxisaturn.ui;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.testfortaxisaturn.R;
import com.example.testfortaxisaturn.utils.Preferences;

import org.joda.time.DateTime;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity {

    @InjectView(R.id.tvExpirationDate)
    TextView tvExpitationDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        Preferences preferences = Preferences.getInstance(this);
        String expDate = preferences.getApiTokenExpirationDate();
        String apiToken = preferences.getApiToken();
        if(TextUtils.isEmpty(expDate) ){
            startLogin();
        } else {
            tvExpitationDate.setText(apiToken + "\n" + expDate);
            DateTime dateTime = new DateTime(expDate);
            DateTime nowDateTime = new DateTime();

            if( dateTime.getMillis() < nowDateTime.getMillis() ){
                startLogin();
            }
        }
    }

    private void startLogin(){
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }
}