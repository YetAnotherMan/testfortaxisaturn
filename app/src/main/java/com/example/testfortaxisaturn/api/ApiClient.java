package com.example.testfortaxisaturn.api;

import android.text.TextUtils;
import android.util.Log;

import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiClient {

    private static final String WEBSOCKETS_URL = "ws://95.213.131.42:8080/customer-gateway/customer";

    private static final String MSG_LOGIN_TYPE = "LOGIN_CUSTOMER";

    private static final String ATTR_TYPE = "type";
    private static final String ATTR_SEQUENCE_ID = "sequence_id";
    private static final String ATTR_DATA = "data";
    private static final String ATTR_EMAIL = "email";
    private static final String ATTR_PASSWORD = "password";

    private static final String SEQ_ID = "715c13b3-881a-9c97-b853-10be585a9747";

    public static void login(final String email, final String pass, final ResponseListener listener){
        AsyncHttpClient.getDefaultInstance().websocket(WEBSOCKETS_URL, "my-prot", new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception e, WebSocket webSocket) {
                if (e != null) {
                    e.printStackTrace();
                    listener.onFailure();
                    return;
                }

                webSocket.send(createJson(email, pass));
                webSocket.setStringCallback(new WebSocket.StringCallback() {
                    @Override
                    public void onStringAvailable(String s) {
                        Log.d(ApiClient.class.getSimpleName(), "s:" + s);
                        if( !TextUtils.isEmpty(s) ){
                            listener.onSuccess(s);
                        }
                    }
                });
            }
        });
    }

    private static String createJson(String email, String pass){
        JSONObject json = new JSONObject();
        try {
            json.put(ATTR_TYPE, MSG_LOGIN_TYPE);
            json.put(ATTR_SEQUENCE_ID, SEQ_ID);

            JSONObject dataObj = new JSONObject();
            dataObj.put(ATTR_EMAIL, email);
            dataObj.put(ATTR_PASSWORD, pass);

            json.put(ATTR_DATA, dataObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(ApiClient.class.getSimpleName(),"json:" + json.toString());
        return json.toString();
    }

    //callback for response status
    public interface ResponseListener {
        void onSuccess(String json);
        void onFailure();
    }
}